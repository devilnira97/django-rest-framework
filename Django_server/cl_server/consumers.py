from channels.generic.websocket import WebsocketConsumer


class ServerConsumer(WebsocketConsumer):
    def connect(self):
        self.accept()
        self.send(text_data='Текст с сервера')

    def disconnect(self, close_code):
        pass

    def receive(self, text_data=None, bytes_data=None):
        self.send(text_data='Hello world!')