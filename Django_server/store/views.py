from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic.edit import FormView
from django.views.generic.base import RedirectView
from django.shortcuts import reverse


from .forms import ProductForm
from .models import Product


class ProductListView(ListView, FormView):
    model = Product
    context_object_name = 'products'
    template_name = 'product_list.html'
    form_class = ProductForm
    success_url = '/api/products/all/'
    paginate_by = 5

    def get_queryset(self):
        price_min = self.request.GET.get('min', '0')
        price_max = self.request.GET.get('max', '10000')
        new_context = Product.objects.filter(product_price__range=[
            price_min, price_max
        ])

        return new_context

    def get_context_data(self, **kwargs):
        context = super(ProductListView, self).get_context_data(**kwargs)
        context['min'] = self.request.GET.get('price_min', '0')
        context['max'] = self.request.GET.get('price_max', '10000')
        return context


class ProductDetailView(DetailView):
    model = Product
    query_pk_and_slug = True
    context_object_name = 'product'
    template_name = 'product_detail.html'


class ProductDetailRedirect(RedirectView):
    permanent = True

    def get_redirect_url(self, pk):
        product = Product.objects.get(pk=pk)
        slug = product.slug
        return reverse('products:product-detail', args=(pk, slug))
