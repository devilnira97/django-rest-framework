from django.apps import AppConfig
from django.db.models.signals import post_save


class StoreConfig(AppConfig):
    name = 'store'

    def ready(self):
        import store.signals
