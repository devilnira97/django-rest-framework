from django.db import models
from django.urls import reverse
# from django.utils.text import slugify
from uuslug import slugify
from sorl.thumbnail import ImageField


class Product(models.Model):
    product_name = models.CharField(
        max_length=255,
        verbose_name='Название товара'
    )
    product_price = models.FloatField(
        max_length=50,
        verbose_name='Цена товара'
    )
    value_discount = models.ForeignKey(
        'Discount',
        on_delete=models.SET_DEFAULT, default='0',
        verbose_name='% скидки',
        blank=True,
        null=True,
    )
    discount_price = models.FloatField(
        max_length=150,
        verbose_name='Цена со скидкой',
        blank=True,
        default='0'
    )
    product_image = models.ImageField(
        upload_to='uploads/images',
        blank=True,
        null=True,
        verbose_name='Фотография товара'
    )
    slug = models.SlugField(
        default='',
        editable=False,
        max_length=150
    )

    def get_absolute_url(self):
        # return reverse('products:product-detail', args=[str(self.id)])
        kwargs = {
            'pk': self.id,
            'slug': self.slug
        }
        return reverse('products:product-detail', kwargs=kwargs)

    @property
    def image_url(self):
        if self.product_image and hasattr(self.product_image, 'url'):
            return self.product_image.url

    def save(self, *args, **kwargs):
        value = self.product_name
        self.slug = slugify(value)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.product_name


class Discount(models.Model):
    value = models.IntegerField(
        verbose_name='Процент скидки'
    )

    # def __int__(self):
    #     return self.id

    def __str__(self):
        return f'Скидка {self.value}'
