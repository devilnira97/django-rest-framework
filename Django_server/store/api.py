from rest_framework import viewsets
from rest_framework.permissions import AllowAny
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response

from .models import Product
from .serializers import ProductSerializer


class ProductViewSet(viewsets.ModelViewSet):
    """Товар"""

    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    permission_classes_by_action = {
        'create': [IsAdminUser],
        'list': [AllowAny]
    }

    def get_permissions(self):
        try:
            return [permission() for
                    permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for
                    permission in self.permission_classes]

    def create(self, request, *args, **kwargs):
        serializer = ProductSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
