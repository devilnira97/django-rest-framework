from django.contrib import admin

from .models import Product
from .models import Discount


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    # fields = ('product_name', 'product_price', 'id', 'slug')
    list_display = (
        'pk',
        'slug',
        'product_name',
        'product_price',
        'value_discount',
        'discount_price',
    )
    readonly_fields = ('discount_price',)
    # list_editable = ('product_name', 'product_price')


@admin.register(Discount)
class DiscountAdmin(admin.ModelAdmin):
    list_display = ('pk', 'value')
