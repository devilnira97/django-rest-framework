from store.celery import celery_app

from .models import Product


@celery_app.task
def refresh():
    product = Product.objects.all()
    for i in product:
        i.discount_price = i.product_price * (100 - i.value_discount.value) / 100
        i.save()
