from rest_framework import status
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from .models import Product, Discount


class ViewTestCase(APITestCase):

    def setUp(self):
        self.superuser = User.objects.create_superuser('admin', 'ad@min', '1234')
        Product.objects.create(
            product_name='Предмет',
            product_price='1000',
            value_discount=None,
            discount_price='0'
        )

    def test_api(self):
        response = self.client.get('/api/products/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_create_403(self):
        data = {'product': {'product_name': 'test', 'product_price': '21'}}
        response = self.client.post('/api/products/', data, format='json')
        self.assertEqual(response.status_code, 403)

    def test_api_detail(self):
        response = self.client.get('/api/products/3/')
        self.assertEqual(response.status_code, 404)

    def test_api_create(self):
        self.client.login(username='admin', password='1234')
        self.data = {'product_name': 'тест',
                     'product_price': '1000',
                     'value_discount': '',
                     'discount_price': '0'
                     }
        response = self.client.post('/api/products/', self.data, format='json')
        self.assertEqual(response.status_code, 201)

    def test_list_view(self):
        response = self.client.get('/store/products/')
        self.assertEqual(response.status_code, 200)

    def test_detail_redirect(self):
        response = self.client.get('/store/product/1')
        self.assertEqual(response.status_code, 301)

    def test_detail_view(self):
        response = self.client.get('/store/product/1-predmet/')
        self.assertEqual(response.status_code, 200)

    def test_update_product(self):
        Discount.objects.create(value='50')
        # Скидка 50% с id=1
        Product.objects.filter(product_name='Предмет').update(value_discount='1')
        self.prod = Product.objects.get(product_name='Предмет')
        self.prod.save()
        self.assertEqual(Product.objects.get(product_name='Предмет').discount_price,
                         500)
