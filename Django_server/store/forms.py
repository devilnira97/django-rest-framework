from django import forms


class ProductForm(forms.Form):
    product_price_max = forms.FloatField()
    product_price_min = forms.FloatField()
