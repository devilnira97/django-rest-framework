from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Product


@receiver(post_save, sender=Product)
def update_discount(sender, instance, **kwargs):
    post_save.disconnect(update_discount, sender=Product)
    product = instance
    if product.value_discount:
        product.discount_price = product.product_price * \
            (100 - product.value_discount.value) / 100
    else:
        product.discount_price = product.product_price
    product.save()
    post_save.connect(update_discount, sender=Product)
