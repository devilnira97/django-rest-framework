from django.urls import path
from django.views.decorators.cache import cache_page

from .views import ProductListView
from .views import ProductDetailView
from .views import ProductDetailRedirect


app_name = 'products'


urlpatterns = [
    path(
        'product/<int:pk>/',
        ProductDetailRedirect.as_view(),
        name='product_redirect'
    ),
    path(
        'products/',
        cache_page(600)(ProductListView.as_view()),
        name='products'
    ),
    path(
        'product/<int:pk>-<str:slug>/',
        cache_page(600)(ProductDetailView.as_view()),
        name='product-detail'
    ),
]
