from rest_framework import serializers

from .models import Product


class ProductSerializer(serializers.ModelSerializer):
    """Сериалайзер для продукта"""

    class Meta:
        model = Product
        fields = [
            'id',
            'product_name',
            'product_price',
            'value_discount',
            'discount_price',
        ]
