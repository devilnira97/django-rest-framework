from .celery import celery_app


default_app_config = 'store.apps.StoreConfig'
__all__ = ['celery_app']
