from django.urls import path
from rest_framework.routers import DefaultRouter

from .api import ProductViewSet


app_name = 'products_api'

router = DefaultRouter()
router.register(r'products', ProductViewSet, basename='anonimous')


urlpatterns = [
] + router.urls
